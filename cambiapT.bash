#!/bin/bash
ayuda () { echo '
#===============================================================================
#           USO:  cambiapT.bash
# 
#   DESCRIPCION:  cambia el papel tapiz
#      OPCIONES:  ---
#  DEPENDENCIAS:  fbsetbg
#          BUGS:  ---
#         NOTAS:  ---
#     VERSIONES:  20110925 0.0.1 PetrOHS Creacion. 
#===============================================================================
' | more;} 

#ayuda
 if [ "$1" = "-h" -o "$1" = "--help" ]
   then ayuda; exit 1;
  elif [ "$1" = "-v" -o "$1" = "--version" ] #version
   then echo "`grep '#     VERSIONES:  ' $0 | grep -v grep | cut -d' ' -f8-10`"; exit 1;
  fi

#lista de imagenes
 file * | grep image | cut -d: -f1 >listaI.txt  ;
 sort listaU.txt                   >listaUt.txt ;
 comm -23 listaI.txt listaUt.txt   >listaIt.txt ;

#obtener nueva imagen
 maxI=`cat listaIt.txt | wc -l`;
 num=$(((RANDOM%$maxI)+1));
 selec=`grep -m $num . listaI.txt | tail -1`;

#cambio de papel tapiz
 fbsetbg -c $selec;

#actualizando lista de utilizados
 tail -4 listaU.txt >listaUt.txt;
 echo "$selec" >> listaUt.txt;
 mv listaUt.txt listaU.txt;

#
 exit 0;
